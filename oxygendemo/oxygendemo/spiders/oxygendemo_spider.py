import json
import scrapy
import requests
import logging
import re

from decimal import Decimal
from collections import defaultdict
from lxml.html import document_fromstring
from oxygendemo.items import OxygenBoutiqueItem

basic_colors = ['white','silver','gray','black','navy','cerulean','sky blue','turquoise','blue green','azure','teal','cyan','green','lime','chartreuse','olive','yellow','gold','amber','orange','brown','orange-red','red','maroon','rose','violet','pink','magenta','purple','blue','peach','apricot', 'ochre','plum']
TWOPLACES = Decimal(10) ** -2
non_decimal = re.compile(r'[^\d.]+')

# rate defaults set on 27 October 2015, will be updated by API if available
USD_RATE = 1.5333
EUR_RATE = 1.3862

# get and update exchange rates
try:
    r = requests.get('http://api.fixer.io/latest?base=GBP')
    rates = json.loads(r.content)
    USD_RATE = rates['rates']['USD']
    EUR_RATE = rates['rates']['EUR']
    logging.info('Rates updated to todays via API: USD: %s EUR: %s' % (USD_RATE, EUR_RATE))
except:
    logging.warning('Rate Lookup Failed, defaults used: USD:%s EUR:%s, set on 27/10/15' % (USD_RATE, EUR_RATE))


def keywithmaxval(d):
     """ Return the dict key with max value - fast method
         a) create a list of the dict's keys and values;
         b) return the key with the max value"""
     v=list(d.values())
     k=list(d.keys())
     return k[v.index(max(v))]

class OxygenBoutiqueSpider(scrapy.Spider):
    """
    Scraper for oxygenboutique.com

    Uses categories as start URLs, with specified accessory URLs so we can
    distinguish accessory type.  A more robust approach would be to parse site cats
    and navigate those, allowing the spider to handle category changes at the
    cost of more lookups.
    """
    name = "oxygenboutique"
    allowed_domains = ["oxygenboutique.com"]
    start_urls = [
        "http://www.oxygenboutique.com/clothing.aspx?ViewAll=1",
        "http://www.oxygenboutique.com/Shoes-All.aspx",
        "http://www.oxygenboutique.com/bracelet.aspx",
        "http://www.oxygenboutique.com/Crystal-Tattoos.aspx",
        "http://www.oxygenboutique.com/earrings.aspx",
        "http://www.oxygenboutique.com/hats.aspx",
        "http://www.oxygenboutique.com/HOMEWEAR.aspx",
        "http://www.oxygenboutique.com/iphone-cases.aspx",
        "http://www.oxygenboutique.com/necklace.aspx",
        "http://www.oxygenboutique.com/ring.aspx",
        "http://www.oxygenboutique.com/Tattoos.aspx",
    ]

    def parse(self, response):
        # get links to crawl, looping twice to add refering URL to callbacks request.meta
        to_parse = {}
        for href in response.css('.itm a::attr(href)').extract():
            to_parse[response.urljoin(href)] = response.url

        for href, referer in to_parse.iteritems():
            request = scrapy.Request(href, callback=self.parse_product)

            # determine whether category is jewellery or bag from url
            if any(cat in referer for cat in ['necklace', 'ring', 'earrings', 'bracelet']):
                request.meta['category'] = 'J'
            if 'bag' in referer:
                request.meta['category'] = 'B'

            request.meta['referer'] = referer.lower()
            yield request

    def parse_product(self, response):
        # Type - try and make a best guess, one of:
        # 'A' apparel, 'S' shoes, 'B' bags, 'J' jewelry, 'R' accessories
        category = response.meta.get('category') if response.meta.get('category') else 'R'
        if 'shoe' in response.meta['referer']:
            category = 'S'
        if 'clothing' in response.meta['referer']:
            category = 'A'
        if 'accessories' in response.meta['referer']:
            category = 'R'

        # easily extractable items
        name = response.xpath('//div[@class="right"]/h2/text()').extract()[0]
        designer = response.xpath('//a[@id="ctl00_ContentPlaceHolder1_AnchorDesigner"]/text()').extract()[0]

        # handle html content
        content_html = response.xpath('//div[@id="accordion"]/div').extract()
        description = document_fromstring(content_html[0]).text_content()

        # sizes and stock are specified within select options
        stock = {}
        sizes = response.xpath('//select[@id="ctl00_ContentPlaceHolder1_ddlSize"]/option/text()').extract()
        sizes.pop(0)
        for size in sizes:
            if 'Sold Out' in size:
                stock[size.split("-")[0].strip()] = 0
            else:
                stock[size] = 1

        # get gbp price and sale prices from html (different markup is used for each)
        sale_discount = None
        try:
            sale_price = ' '.join(document_fromstring(response.xpath('//span[@class="price geo_16_darkbrown"]/span/span').extract()[0]).text_content().split())
            gbp_price = ' '.join(document_fromstring(response.xpath('//span[@class="price geo_16_darkbrown"]/span').extract()[1]).text_content().split())
            gbp_price = non_decimal.sub('', gbp_price)
            sale_discount = sale_price / gbp_price * 100
        except:
            gbp_price = ' '.join(response.xpath('//span[@class="price geo_16_darkbrown"]/text()').extract()[0].split())
            gbp_price = non_decimal.sub('', gbp_price)

        # creating a counter of basic colors matched in either the title or description
        # an alternative would use pillow or other image library to detect colours
        colors = defaultdict(int)
        lowercase_desc = description.lower()
        lowercase_name = name.lower()
        for color in basic_colors:
            if color in lowercase_desc or color in lowercase_name:
                colors[color] += 1

        color_guess = None
        try:
            color_guess = keywithmaxval(colors)
        except ValueError:
            pass

        item = OxygenBoutiqueItem()
        item['code'] = response.url.split('/')[-1].split('.')[0].lower()
        item['description'] = ' '.join(description.split())
        item['designer'] = ' '.join(designer.split())
        item['gender'] = 'F'
        item['image_urls'] = [response.urljoin(href) for href in response.css('#thumbnails-container a::attr(href)').extract()]
        item['link'] = response.url
        item['name'] = ' '.join(name.split())
        item['gbp_price'] = ' '.join(gbp_price.split())
        item['usd_price'] = Decimal(float(item['gbp_price']) * USD_RATE).quantize(TWOPLACES)
        item['eur_price'] = Decimal(float(item['gbp_price']) * EUR_RATE).quantize(TWOPLACES)
        item['raw_color'] = color_guess
        item['sale_discount'] = sale_discount
        #! sizes not standardised e.g. FR8, UK10, IT40
        item['stock_status'] = stock
        item['type'] = category
        yield item
